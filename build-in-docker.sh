#!/bin/bash

set -eu

SDK="/android-home"

BUILD_TOOLS="${SDK}/build-tools/30.0.3"
PLATFORM="${SDK}/platforms/android-33"

mkdir -p build/gen build/obj build/apk

"${BUILD_TOOLS}/aapt" package -f -m -J build/gen/ -S res \
    -M AndroidManifest.xml -I "${PLATFORM}/android.jar"

javac -source 1.8 -target 1.8 -bootclasspath "${JAVA_HOME}/jre/lib/rt.jar" \
    -classpath "${PLATFORM}/android.jar" -d build/obj \
    build/gen/com/example/hello/R.java java/com/example/hello/*.java

"${BUILD_TOOLS}/dx" --dex --output=build/apk/classes.dex build/obj/

"${BUILD_TOOLS}/aapt" package -f -M AndroidManifest.xml -S res/ \
    -I "${PLATFORM}/android.jar" \
    -F build/Hello.unsigned.apk build/apk/

"${BUILD_TOOLS}/zipalign" -f -p 4 \
    build/Hello.unsigned.apk build/Hello.aligned.apk

#keytool -genkeypair -keystore keystore.jks -alias androidkey \
#    -validity 10000 -keyalg RSA -keysize 2048 \
#    -storepass android -keypass android

"${BUILD_TOOLS}/apksigner" sign --ks keystore.jks \
    --ks-key-alias androidkey --ks-pass pass:android \
    --key-pass pass:android --out build/Hello.apk \
    build/Hello.aligned.apk

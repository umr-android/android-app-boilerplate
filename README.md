### Quick start 

```
$ git clone android-app-boilerplate
$ cd android-app-boilerplate
```

#### On first host shell, run

```
$ docker build -t 'umr-android-sdk:1.1' .

$ docker run -t -i -v ./:/android-home/app-source umr-android-sdk:1.1 /bin/bash/

```
The command above try to mount the app source from this repo to the `umr-android-sdk:1.1` docker container <br>
`-v` : volume <br>
`./` : host source <br>
`android-home/app-source` : guest destination <br>
`/bin/bash/` : execute interactive docker shell ( this parameter can be used as an one time execution. E.g. `docker run umr-android-sdk:1.1 /android-home/build-tools/30.0.3/aapt` ) <br>


#### On docker container shell

```
root@<CONTAINER ID>:/android-home/app-source# ./build-in-docker.sh
```

#### On another host shell, run

```
$ cd android-app-boilerplate/build
$ adb install Hello.apk
```